package inf226.Storage;

import inf226.Maybe;

import java.io.*;
import java.sql.*;
import java.util.function.Function;

public class DataBaseUserStorage<C extends Serializable> implements KeyedStorage<String,C>, Closeable {
    private final Id.Generator id_generator;
    private final Function<C,String> computeKey;

    private final Connection dbc;
    private final PreparedStatement lookupKey;
    private final PreparedStatement insertKey;
    private final PreparedStatement updateKey;
    private final PreparedStatement deleteKey;

    public DataBaseUserStorage(final Function<C,String> computeKey) throws SQLException {
        this.id_generator = new Id.Generator();
        this.computeKey = computeKey;

        dbc = DriverManager.getConnection("jdbc:sqlite:userstorage.sqlite");

        Statement createS = dbc.createStatement();
        createS.executeUpdate("CREATE TABLE IF NOT EXISTS c(key TEXT PRIMARY KEY, c BLOB)");
        createS.executeUpdate("CREATE INDEX IF NOT EXISTS keys ON c (key)");

        lookupKey = dbc.prepareStatement("SELECT c FROM c WHERE key == ?");
        insertKey = dbc.prepareStatement("INSERT INTO c (key, c) VALUES (?, ?)");
        updateKey = dbc.prepareStatement("UPDATE c SET c=? WHERE key=?");
        deleteKey = dbc.prepareStatement("DELETE FROM c WHERE key=?");
    }

    public void close() {
        try {
            dbc.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Maybe<Stored<C>> lookup(String key) {
        C c = null;
        Integer id = null;

        try {
            lookupKey.setString(1, key);
            ResultSet rs = lookupKey.executeQuery();
            ResultSet ids = lookupKey.getGeneratedKeys();
            if (rs.next() && ids.next()) {
                c = (C) deserialize(rs.getBytes("c"));
                id = ids.getInt(1);
            }
        } catch (SQLException | IOException e) {
            e.getStackTrace();
            return Maybe.nothing();
        }

        return Maybe.just(new Stored<>(id_generator, c));
    }

    private void executeKV(PreparedStatement ps, C value) throws SQLException {
        ps.setString(1, computeKey.apply(value));
        try {
            ps.setBytes(2, serialize(value));
        } catch (IOException e) {
            throw new SQLException(e);
        }
        ps.executeUpdate();
    }

    @Override
    public Stored<C> save(C value) throws SQLException {
        final Stored<C> stored = new Stored<>(id_generator, value);
        executeKV(insertKey, value);

        return stored;
    }

    @Override
    public Stored<C> update(Stored<C> old, C newValue) throws ObjectModifiedException, ObjectDeletedException, SQLException {
        final Stored<C> stored = new Stored<>(old, newValue);
        executeKV(updateKey, newValue);

        return stored;
    }

    @Override
    public Stored<C> refresh(Stored<C> old) throws ObjectDeletedException, SQLException {
        Maybe<Stored<C>> c = lookup(computeKey.apply(old.getValue()));

        try {
            return c.force();
        } catch (Maybe.NothingException e) {
            throw new ObjectDeletedException(old.id());
        }
    }

    @Override
    public void delete(Stored<C> old) throws ObjectModifiedException, ObjectDeletedException, SQLException {
        deleteKey.setString(1, computeKey.apply(old.getValue()));
        deleteKey.executeUpdate();
    }

    private byte[] serialize(Object object) throws IOException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ObjectOutput out = new ObjectOutputStream(baos);
            out.writeObject(object);
            return baos.toByteArray();
        }
    }

    private Object deserialize(byte[] blob) throws IOException {
        try (ByteArrayInputStream bais = new ByteArrayInputStream(blob)) {
            ObjectInput in = new ObjectInputStream(bais);
            try {
                return in.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}

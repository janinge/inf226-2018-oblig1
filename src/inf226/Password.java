package inf226;

import java.util.stream.Collectors;

public class Password {
    private final String password;

    public Password(String password) {
        if (!validate(password))
            throw new SecurityException("Password doesn't match requirements");
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public static boolean validate(String password) {
        long valid = password.codePoints()
                .mapToObj(cp -> (char)cp)
                .filter(c -> Character.isLetterOrDigit(c)
                        || ".,:;()[]{}<>\\\"\'#!$%&/+*?=-_|".contains(Character.toString(c))
                )
                .collect(Collectors.counting());

        if (password.length() != valid || valid < 6)
            return false;

        return true;
    }
}

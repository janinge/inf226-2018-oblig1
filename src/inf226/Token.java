package inf226;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Base64;

public final class Token implements Serializable {

	private static final long defaultExpiry;

	static {
		defaultExpiry = 1000 * 60 * 60 * 24 * 14; // 14 days
	}

	private final byte[] tokenBits;
	private final Timestamp expires;

	/**
	 * The constructor should generate a random 128 bit token
	 */
	public Token() throws NoSuchAlgorithmException {
		SecureRandom random = SecureRandom.getInstance("NativePRNG");

		tokenBits = new byte[16];
		random.nextBytes(tokenBits);

		expires = new Timestamp(System.currentTimeMillis() + defaultExpiry);
	}
	
	/**
	 * This method should return the Base64 encoding of the token
	 * @return A Base64 encoding of the token
	 */
	public String stringRepresentation() {
		return new String(Base64.getEncoder().encode(tokenBits));
	}

	/**
	 * Checks whether this token still is valid
	 * @return true if still valid
	 */
	public boolean isValid() {
		return expires.compareTo(new Timestamp(System.currentTimeMillis())) > 0;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Token token = (Token) o;
		return Arrays.equals(tokenBits, token.tokenBits);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(tokenBits);
	}
}

package inf226;

import java.lang.ref.WeakReference;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Sets a limit on running threads per source address
 *
 * @author janinge
 */
public class RequestLimiter {
    private final Integer hostLimit;
    private final HashMap<byte[], LinkedList<WeakReference<Thread>>> activeRequests;

    public RequestLimiter(Integer hostLimit) {
        this.hostLimit = hostLimit;
        this.activeRequests = new HashMap<>();
    }

    public boolean accept(RequestProcessor.Request request, InetAddress source) {
        byte[] rawSource = source.getAddress();

        LinkedList<WeakReference<Thread>> list = activeRequests.get(rawSource);

        if (list == null) {
            list = new LinkedList<>();
            activeRequests.put(rawSource.clone(), list);
        }

        if (list.size() < hostLimit || active(list) < hostLimit) {
            list.add(new WeakReference<>(request));
            return true;
        }

        return false;
    }

    /**
     * Count the number of currently active threads
     * 
     * @param threads List with Threads, weakly referenced
     * @return Count of active threads in list
     */
    private int active(List<WeakReference<Thread>> threads) {
        int count = 0;

        for (WeakReference<Thread> weak : threads) {
            Thread t = weak.get();

            // TODO: We should cleanup these periodically, or the list will grow...
            if (t == null || t.getState() == Thread.State.TERMINATED)
                continue;

            count++;
        }

        return count;
    }
}

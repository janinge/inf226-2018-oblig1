package inf226;

import java.io.Serializable;
import java.security.Key;

/**
 * Immutable class for users.
 * @author INF226
 *
 */
public final class User implements Serializable {

	private final String name;

	private final Key password;
	private final byte[] salt;
	private final Integer iterations;

	private final ImmutableLinkedList<Message> log;
	private final ImmutableLinkedList<Token> tokens;

	public User(String name, Key password, byte[] salt, Integer iterations,
				ImmutableLinkedList<Message> log,
				ImmutableLinkedList<Token> tokens) {
		this.name = name;
		this.password = password;
		this.salt = salt;
		this.iterations = iterations;
		this.log = log;
		this.tokens = tokens;
	}

	public User(String name, Key password, byte[] salt, Integer iterations) {
		this.name = name;
		this.password = password;
		this.salt = salt;
		this.iterations = iterations;
		this.log = new ImmutableLinkedList<>();
		this.tokens = new ImmutableLinkedList<>();
	}

	/**
	 *
	 * @return User name
	 */
	public String getName() {
		return name;
	}

	public byte[] getSalt() {
		return salt;
	}

	public Integer getIterations() {
		return iterations;
	}

	public boolean validatePassword(Key password) {
		return this.password.equals(password);
	}

	public boolean validateToken(Token token) {
		for (Token t : this.tokens) {
			if (token.equals(t) && t.isValid())
				return true;
		}

		return false;
	}

	/**
	 * @return Messages sent to this user.
	 */
	public Iterable<Message> getMessages() {
		return log;
	}

	/**
	 * @return Active tokens.
	 */
	public Iterable<Token> getTokens() {
		return tokens;
	}

	/**
	 * Add a message to this user’s log.
	 * @param m Message
	 * @return Updated user object.
	 */
	public User addMessage(Message m) {
		return new User(name, password, salt, iterations, new ImmutableLinkedList<>(m,log), tokens);
	}

	/**
	 * Add authentication token.
	 * @param t Token
	 * @return Updated user object.
	 */
	public User addToken(Token t) {
		return new User(name, password, salt, iterations, log, new ImmutableLinkedList<>(t,tokens));
	}
}

package inf226;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;

public class Message implements Serializable {
	public final String sender, recipient, message;
	
	Message(final String sender, final String recipient, final String message) throws Invalid {
		this.sender = sender;
		this.recipient = recipient;
		if (!valid(message))
			throw new Invalid(message);
		this.message = message;
	}

	/**
	 * Check for invalid control characters or "." on a line by itself
	 * @param message Message to check
	 * @return true if valid, false otherwise
	 */
	public static boolean valid(String message) {
		Optional<Character> invalidCtrl = message
				.codePoints()
				.mapToObj(cp -> (char)cp)
				.filter(c -> Character.isISOControl(c) && Character.getType(c) != Character.LINE_SEPARATOR)
				.findAny();

		if (invalidCtrl.isPresent())
			return false;

		Optional<String> invalidLine = Arrays.stream(message.split("\\R"))
				.filter(line -> line.equals("."))
				.findAny();

		if (invalidLine.isPresent())
			return false;

		return true;
	}

	public static class Invalid extends Exception {
		private static final long serialVersionUID = -3451435075806445718L;

		public Invalid(String msg) {
			super("Invalid string: " + msg);
		}
	}
}

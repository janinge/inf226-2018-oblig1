package inf226;

import java.io.*;
import java.net.*;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;

import inf226.Storage.*;
import inf226.Storage.Storage.ObjectDeletedException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

/**
 * 
 * The Server main class. This implements all critical server functions.
 * 
 * @author INF226
 *
 */
public class Server {
	private static final int portNumber = 1337;
	private static final int kdfIterations = 1337;
	private static final String kdfAlgorithm = "PBKDF2WithHmacSHA256";
	private static final int kdfLength = 256;

	private static final KeyedStorage<String,User> storage;

	static {
		KeyedStorage<String, User> st;
		try {
			st = new DataBaseUserStorage<>(User::getName);
		} catch (SQLException e) {
			e.printStackTrace();
			st = null;
			System.exit(-1);
		}
		storage = st;
	}

	public static Maybe<Stored<User>> authenticate(String username, String password) throws Maybe.NothingException {
		Stored<User> user = storage.lookup(username).force();
		PBEKeySpec keyspec = new PBEKeySpec(
				password.toCharArray(),
				user.getValue().getSalt(),
				user.getValue().getIterations(),
				kdfLength);
		Key stretchedPassword;

		try {
			SecretKeyFactory keyfactory = SecretKeyFactory.getInstance(kdfAlgorithm);
			stretchedPassword = keyfactory.generateSecret(keyspec);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.getStackTrace();
			return Maybe.nothing();
		}

		if (user.getValue().validatePassword(stretchedPassword))
			return Maybe.just(user);

		return Maybe.nothing();
	}

	public static Maybe<Stored<User>> register(String username, String password) {
		byte[] salt = new byte[32];
		SecureRandom srandom;
		SecretKeyFactory keyfactory;

		try {
			srandom = SecureRandom.getInstance("NativePRNGNonBlocking");
			keyfactory = SecretKeyFactory.getInstance(kdfAlgorithm);
		} catch (NoSuchAlgorithmException e) {
			e.getStackTrace();
			return Maybe.nothing();
		}

		srandom.nextBytes(salt);
		PBEKeySpec keyspec = new PBEKeySpec(password.toCharArray(), salt, kdfIterations, kdfLength);
		Key stretchedPassword;

		try {
			stretchedPassword = keyfactory.generateSecret(keyspec);
		} catch (InvalidKeySpecException e) {
			e.getStackTrace();
			return Maybe.nothing();
		}

		User user = new User(username, stretchedPassword, salt, kdfIterations);

		try {
			return Maybe.just(storage.save(user));
		} catch (SQLException e) {
			e.getStackTrace();
			return Maybe.nothing();
		}
	}
	
	public static Maybe<Token> createToken(Stored<User> user) throws SQLException {
		Token token;
		try {
			token = new Token();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return Maybe.nothing();
		}

		storage.save(user.getValue().addToken(token));

		return Maybe.just(token);
	}
	public static Maybe<Stored<User>> authenticate(String username, Token token) throws Maybe.NothingException {
		Stored<User> user = storage.lookup(username).force();

		if (user.getValue().validateToken(token))
			return Maybe.just(user);

		return Maybe.nothing();
	}

	public static boolean sendMessage(Stored<User> sender, Message message) {
		Stored<User> recipient;

		try {
			recipient = storage.lookup(message.recipient).force();
		} catch (Maybe.NothingException e) {
			return false;
		}

		User newRecipient = recipient.getValue().addMessage(message);

		try {
			storage.update(recipient, newRecipient);
		} catch (Storage.ObjectModifiedException | ObjectDeletedException | SQLException e) {
			return false;
		}

		return true;
	}
	
	/**
	 * Refresh the stored user object from the storage.
	 * @param user
	 * @return Refreshed value. Nothing if the object was deleted.
	 */
	public static Maybe<Stored<User>> refresh(Stored<User> user) {
		try {
			return Maybe.just(storage.refresh(user));
		} catch (ObjectDeletedException e) { 
		} catch (SQLException e) { 
		}
		return Maybe.nothing();
	}
	
	/**
	 * @param args TODO: Parse args to get port number
	 */
	public static void main(String[] args) {
		System.setProperty("javax.net.ssl.keyStore", "keystore.jks");
		System.setProperty("javax.net.ssl.keyStorePassword", "U1traS3cure");
		System.setProperty("javax.net.ssl.trustStore", "keystore.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "U1traS3cure");

		final RequestProcessor processor = new RequestProcessor();
		System.out.println("Staring authentication server");
		processor.start();

		try (final SSLServerSocket socket = (SSLServerSocket) SSLServerSocketFactory.getDefault().createServerSocket(portNumber)) {
			socket.setNeedClientAuth(true);

            while(!socket.isClosed()) {
            	System.err.println("Waiting for client to connect…");
        		Socket client = socket.accept();
            	System.err.println("Client connected.");
        		processor.addRequest(new RequestProcessor.Request(client));
			}
		} catch (IOException e) {
			System.out.println("Could not listen on port " + portNumber);
			e.printStackTrace();
		}
	}


}
